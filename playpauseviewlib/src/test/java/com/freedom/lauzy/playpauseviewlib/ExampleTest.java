package com.freedom.lauzy.playpauseviewlib;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleTest {
    @Test
    public void onStart() {
    }
    @Test
    public void testBundleName1() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.freedom.lauzy.playpauseview", actualBundleName);
    }
    @Test
    public void testBundleName2() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.freedom.lauzy.playpauseview", actualBundleName);
    }
    @Test
    public void testBundleName3() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.freedom.lauzy.playpauseview", actualBundleName);
    }
    @Test
    public void testBundleName4() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.freedom.lauzy.playpauseview", actualBundleName);
    }
    @Test
    public void testBundleName5() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.freedom.lauzy.playpauseview", actualBundleName);
    }
}
