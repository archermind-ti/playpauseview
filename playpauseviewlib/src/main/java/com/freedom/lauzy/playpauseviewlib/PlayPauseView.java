package com.freedom.lauzy.playpauseviewlib;

import ohos.aafwk.ability.OnClickListener;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;

public class PlayPauseView extends Component implements Component.EstimateSizeListener, Component.DrawTask {


    private int mWidth; //View宽度
    private int mHeight; //View高度
    private Paint mPaint;
    private Path mLeftPath; //暂停时左侧竖条Path
    private Path mRightPath; //暂停时右侧竖条Path
    private float mGapWidth; //两个暂停竖条中间的空隙,默认为两侧竖条的宽度
    private float mProgress; //动画Progress
    private Rect mRect;
    private boolean isPlaying;
    private float mRectWidth;  //圆内矩形宽度
    private float mRectHeight; //圆内矩形高度
    private float mRectLT;  //矩形左侧上侧坐标
    private float mRadius;  //圆的半径
    private int mBgColor = Color.WHITE.getValue();
    private int mBtnColor = Color.BLACK.getValue();
    private int mDirection = Direction.POSITIVE.value;
    private float mPadding;
    private int mAnimDuration = 200;//动画时间


    public PlayPauseView(Context context) {
        super(context);
    }

    public PlayPauseView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    public PlayPauseView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    public PlayPauseView(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        init(context, attrSet);
    }


    private void init(Context context, AttrSet attrSet) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mLeftPath = new Path();
        mRightPath = new Path();
        mRect = new Rect();

        mBgColor = AttrUtils.getColorFromAttr(attrSet, "bg_color", ResHelper.getColor(context, Color.WHITE.getValue()));
        mBtnColor = AttrUtils.getColorFromAttr(attrSet, "btn_color", ResHelper.getColor(context, Color.BLACK.getValue()));
        mGapWidth = AttrUtils.getDimensionFromAttr(attrSet, "gap_width", AttrHelper.fp2px(0, context));
        mPadding = AttrUtils.getDimensionFromAttr(attrSet, "space_padding", AttrHelper.fp2px(0, context));
        mDirection = AttrUtils.getIntFromAttr(attrSet, "anim_direction", Direction.POSITIVE.value);
        String mode = AttrUtils.getStringFromAttr(attrSet, "anim_direction", null);
        if ("positive".equals(mode)) {
            mDirection = Direction.POSITIVE.value;
        } else if ("negative".equals(mode)) {
            mDirection = Direction.NEGATIVE.value;
        }
        mAnimDuration = AttrUtils.getIntFromAttr(attrSet, "anim_duration", mAnimDuration);


        setEstimateSizeListener(this);

        addDrawTask(this);


    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {

        mWidth = Component.EstimateSpec.getSize(widthMeasureSpec);
        mHeight = Component.EstimateSpec.getSize(heightMeasureSpec);
        int widthMode = Component.EstimateSpec.getMode(widthMeasureSpec);
        int heightMode = Component.EstimateSpec.getMode(heightMeasureSpec);

        if (widthMode == EstimateSpec.PRECISE) {
            mWidth = Math.min(mWidth, mHeight);
        } else {
            mWidth = AttrHelper.vp2px(50, getContext());
        }
        if (heightMode == EstimateSpec.PRECISE) {
            mHeight = Math.min(mWidth, mHeight);
        } else {
            mHeight = AttrHelper.vp2px(50, getContext());
        }
        mWidth = mHeight = Math.min(mWidth, mHeight);

        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(mWidth, widthMode, EstimateSpec.UNCONSTRAINT),
                Component.EstimateSpec.getChildSizeWithMode(mHeight, heightMode, EstimateSpec.UNCONSTRAINT));
        //初始化值
        initValue();
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mLeftPath.rewind();
        mRightPath.rewind();

//        mPaint.setStrokeWidth(1);
//        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(new Color(mBgColor));
        canvas.drawCircle(mWidth / 2, mHeight / 2, mRadius, mPaint);
//        canvas.drawRect(mRect, mPaint);

        float distance = mGapWidth * (1 - mProgress);  //暂停时左右两边矩形距离
        float barWidth = mRectWidth / 2 - distance / 2;     //一个矩形的宽度
        float leftLeftTop = barWidth * mProgress;       //左边矩形左上角

        float rightLeftTop = barWidth + distance;       //右边矩形左上角
        float rightRightTop = 2 * barWidth + distance;  //右边矩形右上角
        float rightRightBottom = rightRightTop - barWidth * mProgress; //右边矩形右下角

        mPaint.setColor(new Color(mBtnColor));
        mPaint.setStyle(Paint.Style.FILL_STYLE);

        if (mDirection == Direction.NEGATIVE.value) {
            mLeftPath.moveTo(mRectLT, mRectLT);
            mLeftPath.lineTo(leftLeftTop + mRectLT, mRectHeight + mRectLT);
            mLeftPath.lineTo(barWidth + mRectLT, mRectHeight + mRectLT);
            mLeftPath.lineTo(barWidth + mRectLT, mRectLT);
            mLeftPath.close();

            mRightPath.moveTo(rightLeftTop + mRectLT, mRectLT);
            mRightPath.lineTo(rightLeftTop + mRectLT, mRectHeight + mRectLT);
            mRightPath.lineTo(rightRightBottom + mRectLT, mRectHeight + mRectLT);
            mRightPath.lineTo(rightRightTop + mRectLT, mRectLT);
        } else {
            mLeftPath.moveTo(leftLeftTop + mRectLT, mRectLT);
            mLeftPath.lineTo(mRectLT, mRectHeight + mRectLT);
            mLeftPath.lineTo(barWidth + mRectLT, mRectHeight + mRectLT);
            mLeftPath.lineTo(barWidth + mRectLT, mRectLT);
            mLeftPath.close();

            mRightPath.moveTo(rightLeftTop + mRectLT, mRectLT);
            mRightPath.lineTo(rightLeftTop + mRectLT, mRectHeight + mRectLT);
            mRightPath.lineTo(rightLeftTop + mRectLT + barWidth, mRectHeight + mRectLT);
            mRightPath.lineTo(rightRightBottom + mRectLT, mRectLT);
        }
        mRightPath.close();

        canvas.save();

        canvas.translate(mRectHeight / 8f * mProgress, 0);

        float progress = isPlaying ? (1 - mProgress) : mProgress;
        int corner = mDirection == Direction.NEGATIVE.value ? -90 : 90;
        float rotation = isPlaying ? corner * (1 + progress) : corner * progress;
        canvas.rotate(rotation, mWidth / 2f, mHeight / 2f);

        canvas.drawPath(mLeftPath, mPaint);
        canvas.drawPath(mRightPath, mPaint);

        canvas.restore();
    }


    public AnimatorValue getPlayPauseAnim() {
        AnimatorValue valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(mAnimDuration);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                if (isPlaying) {
                    mProgress = 1 - value;
                } else {
                    mProgress = value;
                }
                invalidate();
            }
        });

        return valueAnimator;
    }

    public void play() {
        if (getPlayPauseAnim() != null) {
            getPlayPauseAnim().cancel();
        }
        setPlaying(true);
        getPlayPauseAnim().start();
    }

    public void pause() {
        if (getPlayPauseAnim() != null) {
            getPlayPauseAnim().cancel();
        }
        setPlaying(false);
        getPlayPauseAnim().start();
    }

    private PlayPauseListener mPlayPauseListener;

    public void setPlayPauseListener(PlayPauseListener playPauseListener) {
        mPlayPauseListener = playPauseListener;
        setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (isPlaying()) {
                    pause();
                    if (null != mPlayPauseListener) {
                        mPlayPauseListener.pause();
                    }
                } else {
                    play();
                    if (null != mPlayPauseListener) {
                        mPlayPauseListener.play();
                    }
                }
            }
        });
    }


    public interface PlayPauseListener {
        void play();

        void pause();
    }

    private void initValue() {
        mRadius = mWidth / 2;
        mPadding = getSpacePadding() == 0 ? mRadius / 3f : getSpacePadding();
        if (getSpacePadding() > mRadius / Math.sqrt(2) || mPadding < 0) {
            mPadding = mRadius / 3f; //默认值
        }
        float space = (float) (mRadius / Math.sqrt(2) - mPadding); //矩形宽高的一半
        mRectLT = mRadius - space;
        float rectRB = mRadius + space;
        mRect.top = (int) mRectLT;
        mRect.bottom = (int) rectRB;
        mRect.left = (int) mRectLT;
        mRect.right = (int) rectRB;
        mRectWidth = 2 * space;
        mRectHeight = 2 * space;
        mGapWidth = getGapWidth() != 0 ? getGapWidth() : mRectWidth / 3;
        mProgress = isPlaying ? 0 : 1;
        mAnimDuration = getAnimDuration() < 0 ? 200 : getAnimDuration();
    }


    /* ------------下方是参数------------- */

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public void setGapWidth(float gapWidth) {
        mGapWidth = gapWidth;
    }

    public float getGapWidth() {
        return mGapWidth;
    }

    public int getBgColor() {
        return mBgColor;
    }

    public int getBtnColor() {
        return mBtnColor;
    }

    public int getDirection() {
        return mDirection;
    }

    public void setBgColor(int bgColor) {
        mBgColor = bgColor;
    }

    public void setBtnColor(int btnColor) {
        mBtnColor = btnColor;
    }

    public void setDirection(Direction direction) {
        mDirection = direction.value;
    }

    public float getSpacePadding() {
        return mPadding;
    }

    public void setSpacePadding(float padding) {
        mPadding = padding;
    }

    public int getAnimDuration() {
        return mAnimDuration;
    }

    public void setAnimDuration(int animDuration) {
        mAnimDuration = animDuration;
    }


    public enum Direction {
        POSITIVE(1),//顺时针
        NEGATIVE(2);//逆时针
        int value;

        Direction(int value) {
            this.value = value;
        }
    }

}
