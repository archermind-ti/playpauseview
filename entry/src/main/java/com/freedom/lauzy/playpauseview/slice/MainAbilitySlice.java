package com.freedom.lauzy.playpauseview.slice;

import com.freedom.lauzy.playpauseview.ResourceTable;
import com.freedom.lauzy.playpauseviewlib.PlayPauseView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;

import static ohos.agp.utils.LayoutAlignment.BOTTOM;
import static ohos.agp.utils.LayoutAlignment.CENTER;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ToastDialog dialog = new ToastDialog(MainAbilitySlice.this);

        PlayPauseView playPauseView1 = (PlayPauseView) findComponentById(ResourceTable.Id_play_pause_view1);
        playPauseView1.setPlayPauseListener(new PlayPauseView.PlayPauseListener() {
            @Override
            public void play() {
                dialog.setText("Play").setAlignment(BOTTOM | CENTER).show();
            }

            @Override
            public void pause() {
                dialog.setText("Pause").setAlignment(BOTTOM | CENTER).show();
            }
        });


        PlayPauseView playPauseView2 = (PlayPauseView) findComponentById(ResourceTable.Id_play_pause_view2);
        playPauseView2.setPlayPauseListener(new PlayPauseView.PlayPauseListener() {
            @Override
            public void play() {
                dialog.setText("Play").setAlignment(BOTTOM | CENTER).show();
            }

            @Override
            public void pause() {
                dialog.setText("Pause").setAlignment(BOTTOM | CENTER).show();
            }
        });


        Button button = (Button) findComponentById(ResourceTable.Id_btn_paly_pause);
        button.setClickedListener(component -> {
            if (playPauseView1.isPlaying()) {
                playPauseView1.pause();
            } else {
                playPauseView1.play();
            }

            if (playPauseView2.isPlaying()) {
                playPauseView2.pause();
            } else {
                playPauseView2.play();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
