# PlayPauseView

### 简介

让播放、暂停按钮优雅过渡的组件

### 功能
PlayPauseView 让播放、暂停按钮优雅的过渡

### 演示

<img src="/picture/playpause.gif" width="50%" />


### 集成方式

1. 在module的build.gradle中添加对`playpauseview`的依赖

   ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
   	implementation 'com.gitee.archermind-ti:playpauseview:1.0.0'
       ……
   }
   ```

2. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```


### 使用说明

 1. 步骤一：XML布局中初始化控件。

```xml
    <com.freedom.lauzy.playpauseviewlib.PlayPauseView
        ohos:id="$+id:play_pause_view1"
        ohos:height="80vp"
        ohos:width="80vp"
        ohos:layout_alignment="center"
        ohos:top_margin="40vp"
        app:anim_direction="positive"
        app:anim_duration="300"
        app:bg_color="#E0E0E0"
        app:btn_color="#000000"/>


    <com.freedom.lauzy.playpauseviewlib.PlayPauseView
        ohos:id="$+id:play_pause_view2"
        ohos:height="80vp"
        ohos:width="80vp"
        ohos:layout_alignment="center"
        ohos:top_margin="40vp"
        app:anim_direction="negative"
        app:anim_duration="300"
        app:bg_color="#E0E0E0"
        app:btn_color="#000000"/>
```

2. 属性列表：传入不同的属性展示不同的效果。

```java
    mBgColor = AttrUtils.getColorFromAttr(attrSet, "bg_color", ResHelper.getColor(context, Color.WHITE.getValue()));
    mBtnColor = AttrUtils.getColorFromAttr(attrSet, "btn_color", ResHelper.getColor(context, Color.BLACK.getValue()));
    mGapWidth = AttrUtils.getDimensionFromAttr(attrSet, "gap_width", AttrHelper.fp2px(0, context));
    mPadding = AttrUtils.getDimensionFromAttr(attrSet, "space_padding", AttrHelper.fp2px(0, context));
    mDirection = AttrUtils.getIntFromAttr(attrSet, "anim_direction", Direction.POSITIVE.value);
    String mode = AttrUtils.getStringFromAttr(attrSet, "anim_direction", null);
    if ("positive".equals(mode)) {
        mDirection = Direction.POSITIVE.value;
    } else if ("negative".equals(mode)) {
        mDirection = Direction.NEGATIVE.value;
    }
    mAnimDuration = AttrUtils.getIntFromAttr(attrSet, "anim_duration", mAnimDuration);
```

### 编译说明
1.将项目通过git clone 至本地 
2.使用DevEco Studio 打开该项目，然后等待Gradle 构建完成 
3.点击Run运行即可（真机运行可能需要配置签名）


### 版本迭代

* v1.0.0

### 版权和许可信息

* [Apache Licence](LICENSE)